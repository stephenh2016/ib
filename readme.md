Software Engineer Task
This task is intended to be a simple prototype to be completed in a few hours at most. If you are unsure about anything please get in contact.
The Task
Make a mini-dashboard to display information about lights and their events
Fetch data from 2 api sources and display that information using HTML5, CSS and JS (pure JS).
Information which should be conveyed:
The total number of events for each severity (1,2,3) regardless of light
The basic light information for all lights (everything within the light object)
The total number of severe (severity: 3) events per light
The basic event information for those severe events per light (no more than 3)
The 2 sources are:
A list of lights (https://ironbark-task.glitch.me/api/lights.json)
A list of events (https://ironbark-task.glitch.me/api/events.json)
Events relate to lights using the light_id
It is sufficient that reloading the page will reload the data
Deliverables
A link to a js-fiddle with all relevant code.
List of things that were not completed or were unclear
List of assumptions made about the data or users

