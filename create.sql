create table light (id integer primary key autoincrement, light_id varchar(16), color varchar(32), wattage varchar(16), lamp_type varchar(32));
create table event (id integer primary key autoincrement, event_id varchar(16), light_id varchar(16), code varchar(32), severity varchar(1), description varchar(64), duration_sec integer);
