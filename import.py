import sqlite3
import json;
#
# load events and lights into data.sqlite3
#

# read events and lights
with open('./events.json', 'r') as f:
  events = json.load(f)
# print(events)
with open('./lights.json', 'r') as f:
  lights = json.load(f)
# print(lights)

# connect to db
connection = sqlite3.connect("data.sqlite3")
cursor = connection.cursor()

# load events

for event in events:
  cursor.execute('insert into event ( event_id, light_id, code, severity, description,duration_sec) values (?,?,?,?,?,?)',
                                    (event['event_id'],event['light_id'],event['code'],event['severity'],event['description'],event['duration_sec']))
  connection.commit()


# load lights
for light in lights:
  cursor.execute('insert into light (light_id, color, wattage, lamp_type) values (?,?,?,?)',
                                    (light['light_id'],light['color'],light['wattage'],light['lamp_type']))
  connection.commit()
